import enum
import random


class Selection(enum.Enum):
    rock = "камень"
    scissors = "ножницы"
    paper = "бумага"


start_game = input("Вы хотите сыграть в игру (да/нет): ")

if start_game.lower() != "да" and start_game.lower() != "нет":
    print("Некорректный ввод, введите да или нет")
    exit()

if start_game.lower() == "нет":
    exit()

while start_game.lower() == "да":
    user_action = input("Сделайте выбор — камень, ножницы или бумага: ")
    computer_action = random.choice([Selection.rock.value, Selection.scissors.value, Selection.paper.value])
    print(f"\nВы выбрали {user_action}, компьютер выбрал {computer_action}.\n")

    if user_action == computer_action:
        print(f"Оба пользователя выбрали {user_action}. Ничья!!")
    elif user_action == "камень":
        if computer_action == "ножницы":
            print("Камень бьет ножницы! Вы победили!")
        else:
            print("Бумага оборачивает камень! Вы проиграли.")
    elif user_action == "бумага":
        if computer_action == "камень":
            print("Бумага оборачивает камень! Вы победили!")
        else:
            print("Ножницы режут бумагу! Вы проиграли.")
    elif user_action == "ножницы":
        if computer_action == "бумага":
            print("Ножницы режут бумагу! Вы победили!")
        else:
            print("Камень бьет ножницы! Вы проиграли.")
    start_game = input("Вы хотите сыграть в игру (да/нет): ")

